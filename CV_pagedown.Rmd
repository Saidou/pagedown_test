---
title: "Saïdou MAHMOUDOU's resume"
author: Saïdou MAHMOUDOU
date: "`r Sys.Date()`"
output:
  pagedown::html_resume:
    # set it to true for a self-contained HTML page but it'll take longer to render
    self_contained: false
# uncomment this line to produce HTML and PDF in RStudio:
knit: pagedown::chrome_print
---

Aside
================================================================================


![ma_photo](SaidM.jpeg){width=70%}

Contact Info {#contact}
--------------------------------------------------------------------------------

- <i class="fa fa-envelope"></i> yayasaid866@gmail.com
- <i class="fa fa-github"></i> [cermel.org/Saidou](https://cermel.org)
- <i class="fa fa-phone"></i> +24177008059
- For more information, please contact me via email.


Skills {#skills}
--------------------------------------------------------------------------------

- Experienced in statistical analysis, statistical learning models, and optimization methods.

- Full experience with next generation sequencing data analysis.

- Highly skilled in R, Scilab, Matlab, Python, LaTeX

Disclaimer {#disclaimer}
--------------------------------------------------------------------------------

This resume was made with the R package [**pagedown**](https://github.com/rstudio/pagedown).

Last updated on `r Sys.Date()`.

Main
================================================================================

Saidou MAHMOUDOU {#title}
--------------------------------------------------------------------------------

### Currently searching for a PhD student position

I want to do a thesis in the field of biostatistics and clinical trials.

Education {data-icon=graduation-cap data-concise=true}
--------------------------------------------------------------------------------

### Université des Sciences et Techniques de Masuku (USTM)

Msc. in Mathematics obtion Probability-Statistics

Franceville, Gabon

2018

Thesis: The selection of variables in a partially linear regression model

### Université des Sciences et Techniques de Masuku (USTM)

Master's first year in Mathematics

Franceville, Gabon

2017

Thesis: Simulation of a Markov chain with the R software

### Université des Sciences et Techniques de Masuku (USTM)

Bsc. in Mathematics

Franceville, Gabon

2015

Thesis: Deepening of descriptive statistics and programming; Study of multiple linear regression with R and Scilab software

### Université des Sciences et Techniques de Masuku (USTM)

DUS in Mathematics and informatic

Franceville, Gabon

2014

### Lycée Public Simon Oyono Aba’a de Bitam

A-levels

Bitam, Gabon

2012

Research Experience {data-icon=laptop}
--------------------------------------------------------------------------------

### Centre de Recherches Médicales de Lambaréné (CERMEL)

2019 - 2021

Lambaréné, Gabon

- Employed as a research statistician since April 1, 2020.
- A research internship in the Data Management and Statistics department on the management and statistical analysis of medical data under the supervision of Prof. Bertrand LELL (Co-Director), from September 17, 2019 to March 31, 2020.


### Certification

- African Congress of Clinical Trials on the Theme: "2nd Edition The Sub-Saharan Clinical Trial: Challenge or Opportunity?" at CERMEL Lambarene Gabon from November 10 to 14, 2019.
- African Mathematical School AMS) on the theme: "Algorithmic Theory of Numbers and Cryptology, Partial Differential Equations, Numerical Analysis and Scientific Computing" from March 23 to April 3, 2015 and "Mathematics for Cryptography & Robotics" from April 02 to 14, 2018 at USTM

Franceville, Gabon .